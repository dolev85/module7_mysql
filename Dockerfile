FROM openjdk:11
EXPOSE 8080
RUN mkdir -p /home/app
COPY build/libs/bootcamp-java-mysql-project-1.0-SNAPSHOT.jar /home/app
WORKDIR /home/app
CMD ["java", "-jar", "/home/app/bootcamp-java-mysql-project-1.0-SNAPSHOT.jar"]
